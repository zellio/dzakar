

require 'net/smtp'
require 'eventmachine'


module Dzakar

  module Relay
    Intake = EventMachine::Channel.new
    Store = EventMachine::Channel.new
    Broadcast = EventMachine::Channel.new
  end


  class Receiver < EventMachine::Protocols::SmtpServer

    def get_server_greeting
      "dzakar reporting ... ready to serve"
    end

    def get_server_domain
      "mail.zell.io"
    end

    def receive_ehlo_domain domain
      get_server_greeting << ", " << domain
    end

    def receive_sender sender
      # verify sender
      true
    end

    def receive_recipient recipient
      @recipients ||= []
      # verify recipient
      @recipients << recipient

      true
    end

    def receive_reset
      @msg = ""
      @recipeints = nil
    end

    def receive_data_command
      @msg = ""
    end

    def receive_data_chunk data
      @msg << data.join("\n").gsub(/\A\.|(\n)\./, "\\1")
    end

    def receive_message
      message = IntakeMessage.new(@msg)
      message.to = @recipeints

      EventMachine.next_tick { Relay::Intake << message }
      true
    rescue
      p "*** ERROR ****"
      p "  Exception: #{$!}"
      p "  Backtrace:"
      $!.backtrace.each { |l| p "  #{l}" }
      false
    ensure
      receive_reset
    end

    def receive_transaction
    end

  end


  module Broadcast
    include Config::Dispatch::Broadcast

    def list_headers(name)
    end


    def self.call(args)
      list, message = args
      writer = message.writer

      recipients =
        (writer.owns?(list) or list.multicast?) ? list.members : list.owners

      recipient_emails =
        (recipients.collect do |user, permission|
           user.email if user != writer and permission.read?
         end).compact

      Net::SMTP.start(URL, PORT, MASK) do |smtp|
        smtp.send_message message.format(list.name), writer.email, *recipient_emails
      end
    end
  end

end



#msgstr = "From: Your Name <your@mail.address>
#To: Destination Address <someone@example.com>
#Subject: test message
#Date: Sat, 23 Jun 2001 16:26:43 +0900
#Message-Id: <unique.message.id.string@example.com>
#
#This is a test message.
#"
#
#require 'net/smtp'
#Net::SMTP.start('your.smtp.server', 25) do |smtp|
#  smtp.send_message msgstr,
#                    'your@mail.address',
#                    'his_address@example.com'
#end
