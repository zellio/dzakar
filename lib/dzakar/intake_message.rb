

require 'mail'

require 'dzakar/config'

module Dzakar

  class IntakeMessage < Mail::Message
    include Config::IntakeMessage

    TO_REGEX = /([^@]+)@#{HOST_NAME}/

    attr_accessor :writer_email, :list_names

    def initialize(*args, &block)
      super

      @writer_email = from.first.downcase
      @list_names = to.collect do |entry|
        entry[TO_REGEX, 1].downcase
      end
    end

  end

end
