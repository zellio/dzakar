

module Dzakar

  module Config
    HOSTNAME = "zell.io"



    module Application
      RECEIVER_IP = "0.0.0.0"
      RECEIVER_PORT = 25
    end

    module Dispatch
      module Broadcast
        URL = "127.0.0.1"
        PORT = 7895
        MASK = "zell.io"
      end
    end

    module Data

      RESOURCE = "sqlite"
      PATH = "data"
      NAME = "dzakar"
      DB_URI = "#{RESOURCE}://#{PATH}/#{NAME}.db"
      ENCODING = "utf-8"

      module Message
        HOSTNAME = "zell.io"
        HOSTURL = "http://#{HOSTNAME}/list"
        VERSION = "0.5.0"
      end


    end

    module IntakeMessage
      HOST_NAME = "zell.io"
    end

  end
end
