
require 'dzakar/config'
require 'sequel/model'


module Dzakar
  include Config::Data

  DB ||= Sequel.connect(DB_URI, :encoding => ENCODING)

  ##
  # CREATE TABLE users (
  #    id INTEGER PRIMARY KEY AUTOINCREMENT,
  #    guid TEXT NOT NULL UNIQUE DEFAULT(upper(hex(randomblob(16)))),
  #    name TEXT NOT NULL,
  #    email TEXT NOT NULL UNIQUE
  # );
  class User < Sequel::Model
    one_to_many :memberships
    one_to_many :messages, :key => :writer_id

    def owns?(list)
      Membership[:user_id => self.id, :list_id => list.id, :ownership => 1]
    end
  end

  ##
  # CREATE TABLE lists (
  #    id INTEGER PRIMARY KEY AUTOINCREMENT,
  #    guid TEXT NOT NULL UNIQUE DEFAULT(upper(hex(randomblob(16)))),
  #    name TEXT NOT NULL,
  #    parent_id INTEGER NOT NULL,
  #    broadcast_type INTEGER NOT NULL,
  #    FOREIGN KEY (parent_id) REFERENCES lists (id)
  # );
  class List < Sequel::Model
    one_to_many :children, :key => :parent_id, :class => self
    many_to_one :parent,   :key => :parent_id, :class => self
    one_to_many :memberships

    def multicast?
      self.multicast == 1
    end

    def members
      self.memberships.collect { |x| [x.user, x.permission] }
    end

    def owners
      self.members.select { |x, y| x.owns?(self) }
    end
  end


  ##
  # CREATE TABLE messages (
  #    id INTEGER PRIMARY KEY AUTOINCREMENT,
  #    guid TEXT NOT NULL UNIQUE DEFAULT(upper(hex(randomblob(16)))),
  #    message_id TEXT NOT NULL,
  #    subject TEXT,
  #    content TEXT,
  #    writer_id INTEGER NOT NULL,
  #    list_id INTEGER NOT NULL,
  #    reference_id INTEGER,
  #    post_time TEXT NOT NULL DEFAULT(CURRENT_TIMESTAMP),
  #    FOREIGN KEY (writer_id) REFERENCES users (id),
  #    FOREIGN KEY (list_id) REFERENCES lists (id),
  #    FOREIGN KEY (reference_id) REFERENCES messages(id)
  # );
  class Message < Sequel::Model
    include Dzakar::Config::Data::Message

    many_to_one :writer, :key => :writer_id, :class => User
    many_to_one :list

    one_to_many :references, :key => :reference_id, :class => self
    many_to_one :referenced_by,   :key => :reference_id, :class => self



    def reply?
      !!self.referenced_by
    end

    _post_time = instance_method(:post_time)
    define_method(:post_time) do
      Time.parse(_post_time.bind(self).())
    end

    _post_time_equals = instance_method(:post_time=)
    define_method(:post_time=) do | time_obj |
      _post_time_equals.bind(self).(time_obj.to_s)
    end

    def local_message_id
      "#{self.post_time.to_i}.#{self.guid}@#{HOSTNAME}"
    end


    def format(name = 'root')
      unless self.message_id
        self.message_id = "#{(Time.new.to_f * 1000).to_s[/\d+/]}.#{self.guid}@#{HOSTNAME}"
        self.save
      end

      header = <<-EOM
Message-Id: <#{self.local_message_id}>\r
Date: #{Time.now}\r
Reply-To: #{name}@#{HOSTNAME}\r
List-Archive: <#{HOSTURL}/#{name}/archive>\r
List-Help: <#{HOSTURL}/#{name}/help>\r
List-ID: "Dzakar list #{name}" <#{name}.dzakar.#{HOSTNAME}>
List-Owner: <mailto:root@#{HOSTNAME}>\r
List-Post: <mailto:#{name}@#{HOSTNAME}>\r
List-Subscribe: <#{HOSTURL}/#{name}/subscribe>\r
List-Unsubscribe: <#{HOSTURL}/#{name}/unsubscribe>, <mailto:#{name}@#{HOSTNAME}?subject=unsubscribe>\r
EOM

      if self.reply?
        header << "In-Reply-To: #{self.referenced_by.local_message_id}\r\n"
        header << "Subject: #{self.subject}\r\n"
      else
        header << "Subject: [Dzaker.#{name}] #{self.subject}\r\n"
      end

      header << "\r\n" << self.content

    end
  end

  ##
  # CREATE TABLE permissions (
  #    id INTEGER PRIMARY KEY AUTOINCREMENT,
  #    guid TEXT NOT NULL UNIQUE DEFAULT(upper(hex(randomblob(16)))),
  #    name TEXT,
  #    rank INTEGER DEFAULT(127),
  #    read INTEGER DEFAULT(0),
  #    write INTEGER DEFAULT(0),
  #    execute INTEGER DEFAULT(0)
  # );
  class Permission < Sequel::Model
    one_to_many :memberships

    def read?
      self.read == 1
    end

    def write?
      self.write == 1
    end

    def execute?
      self.execute == 1
    end
  end

  ##
  # CREATE TABLE memberships (
  #    id INTEGER PRIMARY KEY AUTOINCREMENT,
  #    user_id INTEGER NOT NULL,
  #    list_id INTEGER NOT NULL,
  #    permission_id INTEGER NOT NULL,
  #    ownership INTEGER NOT NULL DEFAULT(0),
  #    FOREIGN KEY (user_id) REFERENCES users (id),
  #    FOREIGN KEY (list_id) REFERENCES lists (id),
  #    FOREIGN KEY (permission_id) REFERENCES permissions (id)
  # );
  class Membership < Sequel::Model
    many_to_one :user
    many_to_one :list
    many_to_one :permission

    def owner?
      self.ownership == 1
    end
  end

  module Store
    def self.call(mail)

      writer = User[:email => mail.writer_email]

      dispatch_messages = mail.list_names.collect do |name|
        list = List[:name => name]
        reference = Message[:message_id => mail.in_reply_to]

        [list, Message.new(:message_id => mail.message_id,
                           :subject => mail.subject,
                           :content => (mail.text_part || mail).body.decoded,
                           :writer => writer,
                           :list => list,
                           :referenced_by => reference).save]
      end

      EventMachine.next_tick do
        dispatch_messages.each { |message| Relay::Broadcast << message }
      end
    end

  end

end
