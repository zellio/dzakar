



module Dzakar

  class Interpreter

    attr_accessor :message

    def self.call(arg)
      Interpreter.new(arg) do |interpreter|
        EventMachine.next_tick {
          Relay::Store << @message
        }
      end
    end

    def initialize(arg, &block)
      @message = arg

      instance_eval(&block) if block_given?
    end
  end

end
