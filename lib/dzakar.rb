


require 'eventmachine'

require 'dzakar/config'
require 'dzakar/data'
require 'dzakar/intake_message'
require 'dzakar/interpreter'
require 'dzakar/dispatch'


module Dzakar

  class Application
    include Config::Application

    def self.run(*args)
      puts "Starting dzakar ..."


      Relay::Intake.subscribe do |arg|
        puts '>>>> Relay::Intake <<<<'
        puts "  From: #{arg.from} "
        puts "  To: #{arg.to}"
        puts "  Subject: #{arg.subject}"
        puts ""
      end
      Relay::Store.subscribe do |arg|
        puts '>>>> Relay::Store <<<<'
        puts "  Writer: #{arg.writer_email}"
        puts "  Referes: #{arg.in_reply_to}"
        puts ""
        puts ""
      end
      Relay::Broadcast.subscribe do |l, m|
        puts '>>>> Relay::Broadcast <<<<'
        puts "  List: #{l.name}"
        puts "  MGID: #{m.guid}"
        puts ""
        puts ""
      end


      Relay::Intake.subscribe(Interpreter)

      Relay::Store.subscribe(Store)

      Relay::Broadcast.subscribe(Broadcast)

      EventMachine.run do
        EventMachine.start_server RECEIVER_IP, RECEIVER_PORT, Receiver
      end
    end
  end
end
